<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BasicTest extends TestCase
{

    public function testBasicTest()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }
	
    public function testAPIPost1()
    {
 
	$response = $this->postJson('/objects', ['key1' => 'value1']);
        $response->assertStatus(201);
    }
	
    public function testAPIGet1()
    {
	$response = $this->get('/objects/key1');
        $response->assertSee($response['key1']);
    }
	
    public function testAPIPost2()
    {
	sleep(2); // sleep for 2 seconds
	$response = $this->postJson('/objects', ['key1' => 'value2']);
        $response->assertStatus(201);
    }	
	
    public function testAPIGet2()
    {
	$just_now = time() - 1;
	$response = $this->get('/objects/key1?timestamp=' . $just_now);
	$response->assertJson(['key1' => 'value1']);
    }	
	
    public function testAPIGetLongTimeAgo()
    {
	$long_ago = strtotime('1990-01-01 09:00:00');
	$response = $this->get('/objects/key1?timestamp=' . $long_ago);
	$response->assertStatus(200)->assertJson(['success' => 0]);
    }	
	
    public function testAPIGetAllRecords()
    {
	$response = $this->get('/objects/get_all_records');
        $response->assertSee($response['key1']);	
   }     

}
