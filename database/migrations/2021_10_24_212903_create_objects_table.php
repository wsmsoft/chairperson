<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateObjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('objects', function (Blueprint $table) {
	    $table->increments('object_id');
            $table->string('object_key');
	    $table->timestamp('created_on')->useCurrent();
            $table->timestamp('updated_on')->useCurrent();
	});

	Schema::create('objects_history', function (Blueprint $table) {
	    $table->increments('object_history_id');
            $table->integer('object_id');
            $table->text('object_value');
            $table->timestamp('created_on')->useCurrent();
            $table->timestamp('updated_on')->useCurrent();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('objects');
    }
}
