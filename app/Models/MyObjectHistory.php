<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MyObjectHistory extends Model
{
    use HasFactory;
    protected $table = 'objects_history';
    //public $timestamps = false;
    protected $primaryKey = 'object_history_id';	
    protected $fillable = ['object_value'];	
    const CREATED_AT = 'created_on';
    const UPDATED_AT = 'updated_on';	
	
    public function myObject()
    {
       return $this->belongsTo(MyObject::class, 'object_id');
    }		
}
