<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MyObject; 
use App\Models\MyObjectHistory; 
use Carbon\Carbon; 

class ObjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	$data = array();
	foreach (MyObject::all() as $obj) {
	   $objHistory = $obj->myObjectHistory()->orderBy('created_on','DESC')->first();
	   $data[$obj->object_key] = $objHistory->object_value;
	}
	return response()->json($data, 200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$data = json_decode($request->getContent(), true);
	if(count($data)>1)
		return response()->json(['success' => 0, 'error_desc'=>'too many JSON arguments'], 201);
	
	$first_key = key($data);
	$first_val = $data[$first_key];
	$obj = MyObject::firstOrCreate([
		'object_key' => $first_key
	]);
			
	$objHistory = new MyObjectHistory;
	$objHistory->object_value = $first_val;		
	
	$post = $obj->myObjectHistory()->save($objHistory);
	
	return response()->json(['success' => 1, $first_key=>$first_val], 201);		

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
   	$timestamp_query = '';
	if( $request->has('timestamp')) {
		$timestamp_query = $request->query('timestamp');
	}	
	$obj = MyObject::where(['object_key'=>$id])->first();
	
	if(empty($timestamp_query)){
		$objHistory = $obj->myObjectHistory()->orderBy('created_on','DESC')->first();
	} 
	else{
		$objHistory = $obj->myObjectHistory()->where('created_on', '<', date('Y-m-d H:i:s', $timestamp_query))->orderBy('created_on', 'DESC')->first();
		if(is_null($objHistory))
			return response()->json(['success' => 0, 'error_desc'=>'no value found'], 200);
	}
	
	return response()->json([$id =>$objHistory->object_value], 200);		

    }
   
    /*public function update(Request $request, $id)
    {
    }*/

    /*public function destroy($id)
    {
    }*/
}
