<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('objects', 'App\Http\Controllers\ObjectController@index');
Route::get('objects/get_all_records', 'App\Http\Controllers\ObjectController@index');
Route::get('objects/{id}', 'App\Http\Controllers\ObjectController@show');
Route::post('objects', 'App\Http\Controllers\ObjectController@store');

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
